package messages;

import cars.CarId;

public class Crash extends Message {
  private final String msgType = CRASH;
  private final CarId data;
  
  private Crash() {data = null;}
  
  public Crash(CarId dataParam) {
    data = dataParam;
  }
  
  @Override
  public String getMsgType() {
    return msgType;
  }
  
  @Override
  public Object getData() {
    return data;
  }
}



