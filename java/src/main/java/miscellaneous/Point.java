package miscellaneous;

public class Point {
  private double x;
  private double y;
  
  public Point() {}
  
  public Point(double xParam, double yParam) {
    x = xParam;
    y = yParam;
  }
  
  public double getX() {
    return x;
  }
  
  public double getY() {
    return y;
  }
  
  public double distance(Point p) {
    double xdist = x-p.x;
    double ydist = y-p.y;
    return Math.sqrt(xdist*xdist+ydist*ydist);
  }
  
  public void setX(double xParam) {
    x = xParam;
  }
  
  public void setY(double yParam) {
    y = yParam;
  }
  
  @Override
  public String toString() {
    return "{x: " + x + ", y: " + y + "}";
  }
}
