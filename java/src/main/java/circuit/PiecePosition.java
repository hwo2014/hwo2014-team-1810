package circuit;

public class PiecePosition {
  public static class LaneMv {
    public int startLaneIndex;
    public int endLaneIndex;
    @Override
    public String toString() {
      return "{startLaneIndex: " + startLaneIndex + ", endLaneIndex: " + endLaneIndex + "}";
    }
  }
  
  private int pieceIndex;
  private double inPieceDistance;
  private LaneMv lane;
  private int lap;
  
  public PiecePosition() {}
  
  public PiecePosition(int pieceIndexParam, double inPieceDistanceParam, LaneMv laneParam, int lapParam) {
    pieceIndex = pieceIndexParam;
    inPieceDistance = inPieceDistanceParam;
    lane = laneParam;
    lap = lapParam;
  }
  
  public int getPieceIndex() {
    return pieceIndex;
  }
  
  public double getInPieceDistance() {
    return inPieceDistance;
  }
  
  public LaneMv getLane() {
    return lane;
  }
  
  public int getLap() {
    return lap;
  }
  
  public void setPieceIndex(int pieceIndexParam) {
    pieceIndex = pieceIndexParam;
  }
  
  public void setInPieceDistance(double inPieceDistanceParam) {
    inPieceDistance = inPieceDistanceParam;
  }
  
  public void setLane(LaneMv laneParam) {
    lane = laneParam;
  }
  
  public void getLap(int lapParam) {
    lap = lapParam;
  }
  
  @Override
  public String toString() {
    return  "{pieceIndex: " + pieceIndex + ", inPieceDistance: " + inPieceDistance +
            ", lane: " + lane + ", lap: " + lap + "}";
  }
}
