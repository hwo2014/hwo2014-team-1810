package messages;

import races.RaceEndInfo;

public class GameEnd extends Message {
  private final String msgType = GAMEEND;
  private final RaceEndInfo data;
  
  private GameEnd() {data = null;}
  
  public GameEnd(RaceEndInfo dataParam) {
    data = dataParam;
  }
  
  @Override
  public String getMsgType() {
    return msgType;
  }
  
  @Override
  public Object getData() {
    return data;
  }
}



