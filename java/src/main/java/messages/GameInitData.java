package messages;

import java.util.ArrayList;

import cars.Car;
import circuit.Lane;
import circuit.Piece;
import circuit.Straight;
import circuit.Bend;
import circuit.Track;
import races.Race;
import races.RaceSession;
import miscellaneous.PositionAndRotation;

public class GameInitData {

  private static class PieceFields {
    public double length;
    public double radius = Double.POSITIVE_INFINITY;
    public double angle;
    public boolean isSwitch;
    public boolean bridge;
    public boolean isStraight() {
      return radius == Double.POSITIVE_INFINITY;
    }
    @Override
    public String toString() {
      if (radius == Double.POSITIVE_INFINITY) {
        return "{length: " + length + ", switch: " + isSwitch + ", bridge: " +
                bridge + "}";
      } else return "{radius: " + radius + ", angle: " + angle + "}";
    }
  }
  
  private static class MessageTrack {
    public String id;
    public String name;
    public ArrayList<PieceFields> pieces;
    public ArrayList<Lane> lanes;
    public PositionAndRotation startingPoint;
    public Track getTrack() {
      ArrayList<Piece> pieces2 = new ArrayList<Piece>(pieces.size());
      for (PieceFields pfields : pieces) {
        if (pfields.isStraight())
          pieces2.add(new Straight(pfields.length,pfields.isSwitch,pfields.bridge));
        else
          pieces2.add(new Bend(pfields.radius, pfields.angle));
      }
      Track track = new Track(id,name,pieces2,lanes,startingPoint);
      return track;
    }
    @Override
    public String toString() {
      return "{id: \"" + id + "\", name: \"" + name + "\",\npieces: " +
              pieces + ",\nlanes: " + lanes + ",\nstartingPoint: " +
              startingPoint + "}";
    }
  }
  
  private static class MessageRace {
    MessageTrack track;
    public ArrayList<Car> cars;
    public RaceSession raceSession;
    public Race getRace() {
      Race r = new Race(track.getTrack(), cars, raceSession);
      return r;
    }
    @Override
    public String toString() {
      return "{track:\n" + track + ",\ncars: " + cars +
              ",\nraceSession: " + raceSession + "}";
    }
  }
  
  private MessageRace race;
  
  public Race getRace() {
    return race.getRace();
  }
  
  @Override
  public String toString() {
    return "{race: " + race + "}";
  }
  
}

