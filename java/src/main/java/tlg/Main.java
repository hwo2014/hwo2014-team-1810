package tlg;

import java.util.ArrayList;
import java.util.Properties;

public class Main {
  private final static boolean quickRace = true;
  
  public static void main(String[] args) {
    
    String host = args[0];
    int port = Integer.parseInt(args[1]);
    String botName = args[2];
    String botKey = args[3];
    
    if (quickRace) {
      Properties properties = new Properties();
      properties.setProperty("botName", botName);
      properties.setProperty("botKey", botKey);
      properties.setProperty("quickRace", "True");
      Bot bot = new Bot1(true,true,host,port,properties);
      bot.start();
    } else {
      String trackName = "keimola";
      String password = "asd";
      int carCount = 2;
      Properties properties0 = new Properties();
      properties0.setProperty("botName", botName);
      properties0.setProperty("botKey", botKey);
      properties0.setProperty("quickRace", "False");
      properties0.setProperty("trackName", trackName);
      properties0.setProperty("password", password);
      properties0.setProperty("carCount", String.valueOf(carCount));
      new Bot1(true,true,host,port,properties0).start();
    }
  }
}
