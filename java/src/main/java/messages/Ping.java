package messages;

public class Ping extends Message {
  private final String msgType = PING;
  
  @Override
  public String getMsgType() {
    return msgType;
  }
  
  @Override
  public Object getData() {
    return null;
  }
}



