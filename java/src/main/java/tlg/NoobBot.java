package tlg;

import java.util.Properties;
import messages.*;

public class NoobBot extends Bot {
  
  public NoobBot(boolean verboseIn, boolean verboseOut, String hostParam,
  int port, Properties properties) {
    super(verboseIn, verboseOut, hostParam, port, properties);
  }
  
  @Override
  public Message carPositionsFunc(CarPositions msg) {
    return new Throttle(0.5);
  }
  
  @Override
  public Message crashFunc(Crash msg) {
    return null;
  }
  
  @Override
  public Message dnfFunc(Disqualified msg) {
    return null;
  }
  
  @Override
  public Message finishFunc(Finish msg) {
    return null;
  }
  
  @Override
  public Message gameEndFunc(GameEnd msg) {
    return null;
  }
  
  @Override
  public Message gameInitFunc(GameInit msg) {
    return null;
  }
  
  @Override
  public Message gameStartFunc(GameStart msg) {
    return new Throttle(0.5);
  }
  
  @Override
  public Message joinFunc(Join msg) {
    return null;
  }
  
  @Override
  public Message lapFinishedFunc(LapFinished msg) {
    return null;
  }
  
  @Override
  public Message spawnFunc(Spawn msg) {
    return null;
  }
  
  @Override
  public Message tournamentEndFunc(TournamentEnd msg) {
    return null;
  }
  
  @Override
  public Message turboAvailableFunc(TurboAvailable msg) {
    return null;
  }
  
  @Override
  public Message turboEndFunc(TurboEnd msg) {
    return null;
  }
  
  @Override
  public Message turboStartFunc(TurboStart msg) {
    return null;
  }
  
  @Override
  public Message yourCarFunc(YourCar msg) {
    return null;
  }
  
  @Override
  public Message otherFunc(Other msg) {
    return null;
  }
}
