package messages;

public class TournamentEnd extends Message {
  public final String msgType = TOURNAMENTEND;
  public final Object data = null;
  
  @Override
  public String getMsgType() {
    return msgType;
  }
  
  @Override
  public Object getData() {
    return data;
  }
}
