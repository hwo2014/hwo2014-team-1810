package messages;

import cars.CarId;

public class YourCar extends Message {
  private final String msgType = YOURCAR;
  private final CarId data;
  
  private YourCar() {data = null;}
  
  public YourCar(CarId dataParam) {
    data = dataParam;
  }
  
  @Override
  public String getMsgType() {
    return msgType;
  }
  
  @Override
  public Object getData() {
    return data;
  }
}

