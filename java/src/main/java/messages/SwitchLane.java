package messages;

public class SwitchLane extends Message {
  private final String msgType = SWITCHLANE;
  private final String data;
  
  private SwitchLane() {data = null;}
  
  public SwitchLane(String dataParam) {
    data = dataParam;
  }
  
  @Override
  public String getMsgType() {
    return msgType;
  }
  
  @Override
  public Object getData() {
    return data;
  }
}
