package circuit;

public class Lane {
  private double distanceFromCenter;
  private int index;
  
  public Lane() {}
  
  public Lane(double distFromCenterParam, int indexParam) {
    distanceFromCenter = distFromCenterParam;
    index = indexParam;
  }
  
  public double getDistanceFromCenter() {
    return distanceFromCenter;
  }
  
  public int getIndex() {
    return index;
  }
  
  public void setDistanceFromCenter(double distFromCenterParam) {
    distanceFromCenter = distFromCenterParam;
  }
  
  public void setIndex(int indexParam) {
    index = indexParam;
  }
  
  public String toString() {
    return "{distanceFromCenter: " + distanceFromCenter + ", index: " + index + "}";
  }
}
