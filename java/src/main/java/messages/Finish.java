package messages;

import cars.CarId;

public class Finish extends Message {
  private final String msgType = FINISH;
  private final CarId data;
  
  private Finish() {data = null;}
  
  public Finish(CarId dataParam) {
    data = dataParam;
  }
  
  @Override
  public String getMsgType() {
    return msgType;
  }
  
  @Override
  public Object getData() {
    return data;
  }
}



