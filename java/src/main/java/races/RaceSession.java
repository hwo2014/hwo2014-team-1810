package races;

public class RaceSession {
  private final int durationMs;
  private final int laps;
  private final int maxLapTimeMs;
  private final boolean quickRace;
  
  private RaceSession() {durationMs = laps = maxLapTimeMs = 0; quickRace = false;}
  
  public RaceSession(int durationMsParam, int lapsParam, int maxLapTimeMsParam,
  boolean quickRaceParam) {
    durationMs = durationMsParam;
    laps = lapsParam;
    maxLapTimeMs = maxLapTimeMsParam;
    quickRace = quickRaceParam;
  }
  
  public int getDurationMs() {
    return durationMs;
  }
  
  public int getLaps() {
    return laps;
  }
  
  public int getMaxLapTimeMs() {
    return maxLapTimeMs;
  }
  
  public boolean isQuickRace() {
    return quickRace;
  }
  
  public boolean isQualifyingPhase() {
    return durationMs == 0;
  }
  
  @Override
  public String toString() {
    return "{laps: " + laps + ", maxLapTimeMs: " + maxLapTimeMs + ", quickRace: " + quickRace + "}";
  }
}
