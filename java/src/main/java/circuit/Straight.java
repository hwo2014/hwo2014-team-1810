package circuit;

public class Straight extends Piece {
  private double length;
  private boolean isSwitch;
  private boolean isBridge;
  
  public Straight() {}
  
  public Straight(double lengthParam, boolean isSwitchParam, boolean isBridgeParam) {
    length = lengthParam;
    isSwitch = isSwitchParam;
    isBridge = isBridgeParam;
  }
  
  @Override
  public double length() {
    return length;
  }
  
  @Override
  public int getType() {
    return STRAIGHT;
  }
  
  public boolean isSwitch() {
    return isSwitch;
  }
  
  public boolean isBridge() {
    return isBridge;
  }
  
  public void setLength(int lengthParam) {
    length = lengthParam;
  }
  
  public void setSwitch(boolean isSwitchParam) {
    isSwitch = isSwitchParam;
  }
  
  public void setBridge(boolean isBridgeParam) {
    isBridge = isBridgeParam;
  }
  
  @Override
  public String toString() {
    return "{type: straight, length: " + length + ", switch: " + isSwitch + 
            ", bridge: " + isBridge + "}";
  }
}
