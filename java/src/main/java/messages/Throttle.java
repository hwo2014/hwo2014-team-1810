package messages;

public class Throttle extends Message {
  private final String msgType = THROTTLE;
  private final Double data;
  
  private Throttle() {data = null;}
  
  public Throttle(Double dataParam) {
    data = dataParam;
  }
  
  @Override
  public String getMsgType() {
    return msgType;
  }
  
  @Override
  public Object getData() {
    return data;
  }
}


