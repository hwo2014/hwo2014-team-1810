package messages;

public class Disqualified extends Message {
  private final String msgType = DNF;
  private final DisqualifiedData data;
  
  private Disqualified() {data = null;}
  
  public Disqualified(DisqualifiedData dataParam) {
    data = dataParam;
  }
  
  @Override
  public String getMsgType() {
    return msgType;
  }
  
  @Override
  public Object getData() {
    return data;
  }
}



