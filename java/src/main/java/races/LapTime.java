package races;

public class LapTime {
  private final int lap;
  private final int ticks;
  private final int millis;
  
  private LapTime() {lap = ticks = millis = 0;}
  
  public LapTime(int lapsParam, int ticksParam, int millisParam) {
    lap = lapsParam;
    ticks = ticksParam;
    millis = millisParam;
  }
  
  public int getLaps() {
    return lap;
  }
  
  public int getTicks() {
    return ticks;
  }
  
  public int getMillis() {
    return millis;
  }
  
  @Override
  public String toString() {
    return "{lap: " + lap + ", ticks: " + ticks + ", millis: " + millis + "}";
  }
}
