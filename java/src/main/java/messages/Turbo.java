package messages;

public class Turbo extends Message {
  private final String msgType = TURBO;
  private final String data;
  
  private Turbo() {data = null;}
  
  public Turbo(String dataParam) {
    data = dataParam;
  }
  
  @Override
  public String getMsgType() {
    return msgType;
  }
  
  @Override
  public Object getData() {
    return data;
  }
}
