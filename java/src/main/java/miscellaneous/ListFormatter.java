package miscellaneous;

import java.util.List;

public class ListFormatter {
  private final String str;
  
  public ListFormatter(List<Double> list, String name) {
    StringBuilder builder = new StringBuilder();
    builder .append("#name: " + name + "\n")
            .append("#type: matrix\n")
            .append("#rows: 1\n")
            .append("#columns: " + list.size() + "\n");
    for (Double d : list)
      builder.append(" " + d + "\n");
    str = builder.toString();
  }
  
  @Override
  public String toString() {
    return str;
  }
}
