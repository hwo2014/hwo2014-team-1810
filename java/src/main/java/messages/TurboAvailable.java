package messages;

public class TurboAvailable extends Message {
  private final String msgType = TURBOAVAILABLE;
  private final TurboAvailabilityData data;
  
  private TurboAvailable() {data = null;}
  
  public TurboAvailable(TurboAvailabilityData dataParam) {
    data = dataParam;
  }
  
  @Override
  public String getMsgType() {
    return msgType;
  }
  
  @Override
  public Object getData() {
    return data;
  }
}
