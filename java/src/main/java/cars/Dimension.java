package cars;

public class Dimension {
  private double length;
  private double width;
  private double guideFlagPosition;
  
  public Dimension() {}
  
  public Dimension(double lengthParam, double widthParam, double guideFlagPositionParam) {
    length = lengthParam;
    width = widthParam;
    guideFlagPosition = guideFlagPositionParam;
  }
  
  public double getLength() {
    return length;
  }
  
  public double getWidth() {
    return width;
  }
  
  public double getGuideFlagPosition() {
    return guideFlagPosition;
  }
  
  @Override
  public String toString() {
    return "{length: " + length + ", width: " + width + ", guideFlagPosition: " +
            guideFlagPosition + "}";
  }
}
