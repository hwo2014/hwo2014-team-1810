package messages;

import cars.CarId;

public class TurboStart extends Message {
  private final String msgType = TURBOSTART;
  private final CarId data;
  
  private TurboStart() {data = null;}
  
  public TurboStart(CarId dataParam) {
    data = dataParam;
  }
  
  @Override
  public String getMsgType() {
    return msgType;
  }
  
  @Override
  public Object getData() {
    return data;
  }
}


