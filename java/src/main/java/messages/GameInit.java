package messages;

public class GameInit extends Message {
  private final String msgType = GAMEINIT;
  private final GameInitData data;
  
  private GameInit() {data = null;}
  
  public GameInit(GameInitData dataParam) {
    data = dataParam;
  }
  
  @Override
  public String getMsgType() {
    return msgType;
  }
  
  @Override
  public Object getData() {
    return data.getRace();
  }
}
