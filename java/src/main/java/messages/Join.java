package messages;

public class Join extends Message {
  private final String msgType = JOIN;
  private final BotId data;
  
  private Join() {data = null;}
  
  public Join(BotId dataParam) {
    data = dataParam;
  }
  
  public Join(String nameBot, String keyBot) {
    data = new BotId(nameBot,keyBot);
  }
  
  @Override
  public String getMsgType() {
    return msgType;
  }
  
  @Override
  public Object getData() {
    return data;
  }
}
