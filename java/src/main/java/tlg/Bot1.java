package tlg;

import java.util.ArrayList;
import java.util.Properties;
import circuit.*;
import cars.*;
import races.*;
import messages.*;

public class Bot1 extends Bot {
  
  private final int ticksToEstimateValues = 6; // minimum 3
  
  private CarId myCar;
  private Race race;
  private double currentThrottle = 0.6;
  
  private double K;
  private double velThrottle;
  
  public Bot1(boolean verboseIn, boolean verboseOut, String hostParam,
  int port, Properties properties) {
    super(verboseIn, verboseOut, hostParam, port, properties);
  }
  
  @Override
  public Message carPositionsFunc(CarPositions msg) {
    Message answer = new Throttle(currentThrottle);;
    ArrayList<CarPosition> positions = (ArrayList<CarPosition>)msg.getData();
    for (CarPosition pos : positions) race.updatePosition(pos);
    if (race.getRaceSession().isQualifyingPhase() && getGameTick() < ticksToEstimateValues) {
      ArrayList<Double> myCarSpeeds = race.getCar(myCar).getSpeedEvolution();
      if (myCarSpeeds.size() >= 3) {
        int n = myCarSpeeds.size() - 1;
        double vn = myCarSpeeds.get(n);
        double vn_1 = myCarSpeeds.get(n-1);
        double vn_2 = myCarSpeeds.get(n-2);
        double KNew = (vn-vn_1)/(vn_1-vn_2);
        double velThrottleNew = (vn*vn_2-vn_1*vn_1)/(vn-2*vn_1+vn_2)/currentThrottle;
        K = (K*(n-2)+KNew)/(n-1);
        velThrottle = (velThrottle*(n-2)+velThrottleNew)/(n-1);
      }
      System.out.println("Estimated K: " + K);
      System.out.println("Estimated velThrottle: " + velThrottle);
    }
    answer.setGameTick(getGameTick());
    return answer;
  }
  
  @Override
  public Message crashFunc(Crash msg) {
    return null;
  }
  
  @Override
  public Message dnfFunc(Disqualified msg) {
    return null;
  }
  
  @Override
  public Message finishFunc(Finish msg) {
    return null;
  }
  
  @Override
  public Message gameEndFunc(GameEnd msg) {
    return null;
  }
  
  @Override
  public Message gameInitFunc(GameInit msg) {
    race = (Race)msg.getData();
    return null;
  }
  
  @Override
  public Message gameStartFunc(GameStart msg) {
    Message answer = new Throttle(currentThrottle);
    answer.setGameTick(getGameTick());
    return answer;
  }
  
  @Override
  public Message joinFunc(Join msg) {
    return null;
  }
  
  @Override
  public Message lapFinishedFunc(LapFinished msg) {
    return null;
  }
  
  @Override
  public Message spawnFunc(Spawn msg) {
    return null;
  }
  
  @Override
  public Message tournamentEndFunc(TournamentEnd msg) {
    for (Car car : race.getCars())
      car.printDataToFile(car.getId().getName().replace(" ","_")+".txt");
    return null;
  }
  
  @Override
  public Message turboAvailableFunc(TurboAvailable msg) {
    return null;
  }
  
  @Override
  public Message turboEndFunc(TurboEnd msg) {
    return null;
  }
  
  @Override
  public Message turboStartFunc(TurboStart msg) {
    return null;
  }
  
  @Override
  public Message yourCarFunc(YourCar msg) {
    myCar = (CarId)msg.getData();
    return null;
  }
  
  @Override
  public Message otherFunc(Other msg) {
    return null;
  }
}
