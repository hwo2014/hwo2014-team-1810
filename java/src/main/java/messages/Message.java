package messages;

import java.util.Scanner;
import com.google.gson.Gson;

public abstract class Message {
  //// CONSTANTS ////
  
  // SERVER MESSAGES
  public static final String CARPOSITIONS = "carPositions";
  public static final String CRASH = "crash";
  public static final String DNF = "dnf";
  public static final String FINISH = "finish";
  public static final String GAMEEND = "gameEnd";
  public static final String GAMEINIT = "gameInit";
  public static final String GAMESTART = "gameStart";
  public static final String LAPFINISHED = "lapFinished";
  public static final String SPAWN = "spawn";
  public static final String TOURNAMENTEND = "tournamentEnd";
  public static final String TURBOAVAILABLE = "turboAvailable";
  public static final String TURBOEND = "turboEnd";
  public static final String TURBOSTART = "turboStart";
  public static final String YOURCAR = "yourCar";
  
  // BOT MESSAGES
  public static final String JOIN = "join"; // also server message
  public static final String PING = "ping";
  public static final String SWITCHLANE = "switchLane";
  public static final String THROTTLE = "throttle";
  public static final String TURBO = "turbo";
  
  // RACE MANAGEMENT MESSAGES
  public static final String CREATERACE = "createRace";
  public static final String JOINRACE = "joinRace";
  
  //// END CONSTANTS ////
  
  private static final Gson gson = new Gson();
  
  public static String extractTypeFromJson(String json) {
    Scanner sc = new Scanner(json);
    String fieldName = sc.findInLine("\"msgType\"");
    String fieldValue = null;
    if (fieldName != null) fieldValue = sc.findInLine("\"[A-Za-z]*\"");
    sc.close();
    if (fieldValue != null) 
      return fieldValue.substring(1,fieldValue.length()-1);
    else return "unknownMessage";
  }
  
  public static Message fromJson(String json) {
    Message msg;
    String type = extractTypeFromJson(json);
    switch (type) {
      case CARPOSITIONS: msg = gson.fromJson(json,CarPositions.class); break;
      case CRASH: msg = gson.fromJson(json,Crash.class); break;
      case DNF: msg = gson.fromJson(json,Disqualified.class); break;
      case FINISH: msg = gson.fromJson(json,Finish.class); break;
      case GAMEEND: msg = gson.fromJson(json,GameEnd.class); break;
      case GAMEINIT: msg = gson.fromJson(json.replace("switch","isSwitch"),GameInit.class); break;
      case GAMESTART: msg = gson.fromJson(json,GameStart.class); break;
      case JOIN: msg = gson.fromJson(json, Join.class); break;
      case LAPFINISHED: msg = gson.fromJson(json, LapFinished.class); break;
      case SPAWN: msg = gson.fromJson(json, Spawn.class); break;
      case TOURNAMENTEND: msg = gson.fromJson(json, TournamentEnd.class); break;
      case TURBOAVAILABLE: msg = gson.fromJson(json,TurboAvailable.class); break;
      case TURBOSTART: msg = gson.fromJson(json,TurboStart.class); break;
      case TURBOEND: msg = gson.fromJson(json,TurboEnd.class); break;
      case YOURCAR: msg = gson.fromJson(json,YourCar.class); break;
      default: msg = gson.fromJson(json,Other.class);
    }
    return msg;
  }
  
  private String gameId;
  private Integer gameTick;
  
  public abstract String getMsgType();
  
  public abstract Object getData();
  
  public String getGameId() {
    return gameId;
  }
  
  public Integer getGameTick() {
    return gameTick;
  }
  
  public void setGameId(String gameIdParam) {
    gameId = gameIdParam;
  }
  
  public void setGameTick(Integer gameTickParam) {
    gameTick = gameTickParam;
  }
  
  public String toJson() {
    return gson.toJson(this);
  }
  
  @Override
  public String toString() {
    StringBuilder sbuild = new StringBuilder();
    sbuild  .append("{msgType: \"").append(getMsgType()).append("\", ")
            .append("data: {\n")
            .append(getData())
            .append("},\ngameId: \"").append(gameId).append("\", ")
            .append("gameTick: ").append(gameTick)
            .append("}\n");
    return sbuild.toString();
  }
}
