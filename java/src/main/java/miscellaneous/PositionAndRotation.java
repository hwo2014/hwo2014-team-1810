package miscellaneous;

public class PositionAndRotation {
  private Point position;
  private double angle;

  public PositionAndRotation() {}
  
  public PositionAndRotation(Point positionParam, double angleParam) {
    position = positionParam;
    angle = angleParam;
  }
  
  public Point getPosition() {
    return position;
  }
  
  public double getAngle() {
    return angle;
  }
  
  @Override
  public String toString() {
    return "{position: " + position + ", angle: " + angle + "}";
  }
}
