package circuit;

public abstract class Piece {
  public final static int BEND = 1;
  public final static int STRAIGHT = 2;
  public abstract int getType();
  public abstract double length();
}
