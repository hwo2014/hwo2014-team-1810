package circuit;

import java.util.ArrayList;

import miscellaneous.PositionAndRotation;

public class Track {
  private final String id;
  private final String name;
  private final ArrayList<Piece> pieces;
  private final ArrayList<Lane> lanes;
  private final PositionAndRotation startingPoint;
  private double centerLength;
  
  public Track(String idParam, String nameParam, ArrayList<Piece> piecesParam,
  ArrayList<Lane> lanesParam, PositionAndRotation startingPointParam) {
    name = nameParam;
    id = idParam;
    pieces = piecesParam;
    lanes = lanesParam;
    startingPoint = startingPointParam;
    centerLength = 0;
    for (Piece p : pieces) centerLength += p.length();
  }
  
  public String getName() {
    return name;
  }
  
  public String getId() {
    return id;
  }
  
  public ArrayList<Piece> getPieces() {
    return pieces;
  }
  
  public ArrayList<Lane> getLanes() {
    return lanes;
  }
  
  public double getLaneCenterDistance(int index) {
    for (Lane l : lanes)
      if (l.getIndex() == index) return l.getDistanceFromCenter();
    return 0;
  }
  
  public PositionAndRotation getStartingPoint() {
    return startingPoint;
  }
  
  public double getCenterLength() {
    return centerLength;
  }
  
  //public double getLaneLength(int index) {
  //}
  
  @Override
  public String toString() {
    return "{id: \"" + id + "\", name: \"" + name + "\",\npieces: " +
            pieces + ",\nlanes: " + lanes + ",\nstartingPoint: " +
            startingPoint + "}";
  }
}
