package cars;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import circuit.*;
import miscellaneous.ListFormatter;
import races.Race;

public class Car {
  private final CarId id;
  private final Dimension dimensions;
  
  private Race race;
  
  private PiecePosition position;
  private final ArrayList<Double> angleEvolution =
    new ArrayList<Double>(4000);
  private final ArrayList<Double> distanceEvolution =
    new ArrayList<Double>(4000);
  private final ArrayList<Double> speedEvolution =
    new ArrayList<Double>(4000);
  private final ArrayList<Double> accelerationEvolution =
    new ArrayList<Double>(4000);
  private boolean crashed;
  private boolean disqualified;
  
  private Car() {id = null; dimensions = null;}
  
  public Car(CarId idParam, Dimension dimensionsParam) {
    id = idParam;
    dimensions = dimensionsParam;
  }
  
  public CarId getId() {
    return id;
  }
  
  public Dimension getDimensions() {
    return dimensions;
  }
  
  public PiecePosition getPosition() {
    return position;
  }
  
  public ArrayList<Double> getAngleEvolution() {
    return angleEvolution;
  }
  
  public ArrayList<Double> getDistanceEvolution() {
    return distanceEvolution;
  }
  
  public ArrayList<Double> getSpeedEvolution() {
    return speedEvolution;
  }
  
  public ArrayList<Double> getAccelerationEvolution() {
    return accelerationEvolution;
  }
  
  public double getCurrentDistance() {
    return distanceEvolution.get(distanceEvolution.size()-1);
  }
  
  public double getCurrentSpeed() {
    return speedEvolution.get(speedEvolution.size()-1);
  }
  
  public double getCurrentAcceleration() {
    return accelerationEvolution.get(accelerationEvolution.size()-1);
  }
  
  public double getCurrentAngle() {
    return angleEvolution.get(angleEvolution.size()-1);
  }
  
  public boolean isCrashed() {
    return crashed;
  }
  
  public boolean isDisqualified() {
    return disqualified;
  }
  
  public void setCrashed(boolean crashedParam) {
    crashed = crashedParam;
  }
  
  public void setDisqualified(boolean disqualifiedParam) {
    disqualified = disqualifiedParam;
  }
  
  public void setRace(Race raceParam) {
    position = null;
    angleEvolution.clear();
    distanceEvolution.clear();
    speedEvolution.clear();
    accelerationEvolution.clear();
    race = raceParam;
  }
  
  public void updatePosition(PiecePosition newPosition, double angle) {
    if (distanceEvolution.isEmpty()) {
      distanceEvolution.add(0.0);
      speedEvolution.add(0.0);
      accelerationEvolution.add(0.0);
    }
    else if (newPosition.getPieceIndex() == position.getPieceIndex()) {
      double inc = newPosition.getInPieceDistance() - position.getInPieceDistance();
      distanceEvolution.add(distanceEvolution.get(distanceEvolution.size()-1)+inc);
      speedEvolution.add(inc);
      accelerationEvolution.add(inc-speedEvolution.get(speedEvolution.size()-2));
    }
    else {
      Piece previousPiece = race.getTrack().getPieces().get(position.getPieceIndex());
      double inc = newPosition.getInPieceDistance() - position.getInPieceDistance();
      if (previousPiece.getType() == Piece.STRAIGHT) inc += previousPiece.length();
      else {
        double distCenter = race.getTrack().getLaneCenterDistance(position.getLane().endLaneIndex);
        inc += ((Bend)previousPiece).length(distCenter);
      }
      distanceEvolution.add(distanceEvolution.get(distanceEvolution.size()-1)+inc);
      speedEvolution.add(inc);
      accelerationEvolution.add(inc-speedEvolution.get(speedEvolution.size()-2));
    }
    angleEvolution.add(angle);
    position = newPosition;
  }
  
  public void printDataToFile(String filename) {
    try (PrintWriter writer = new PrintWriter(filename)) {
      ListFormatter lformat = new ListFormatter(distanceEvolution,"Distance");
      writer.println(lformat);
      lformat = new ListFormatter(speedEvolution,"Speed");
      writer.println(lformat);
      lformat = new ListFormatter(accelerationEvolution,"Acceleration");
      writer.println(lformat);
      lformat = new ListFormatter(angleEvolution,"Angle");
      writer.println(lformat);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  @Override
  public String toString() {
    return "{id: " + id + ", dimensions: " + dimensions + "}";
  }
}
