package messages;

public class GameStart extends Message {
  public final String msgType = GAMESTART;
  public final Object data = null;
  
  @Override
  public String getMsgType() {
    return msgType;
  }
  
  @Override
  public Object getData() {
    return data;
  }
}
