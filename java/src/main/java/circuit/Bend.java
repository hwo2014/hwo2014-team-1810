package circuit;

public class Bend extends Piece {
  private double radius;
  private double angle;
  
  public Bend() {}
  
  public Bend(double radiusParam, double angleParam) {
    radius = radiusParam;
    angle = angleParam;
  }
  
  public double getRadius() {
    return radius;
  }
  
  public double getAngle() {
    return angle;
  }
  
  @Override
  public int getType() {
    return BEND;
  }
  
  public double length(double distCenter) {
    if (angle < 0) return -Math.PI/180.0*angle*(radius+distCenter);
    else return Math.PI/180.0*angle*(radius-distCenter);
  }
  
  @Override
  public double length() {
    return Math.PI/180.0*Math.abs(angle)*radius;
  }
  
  public void setRadius(double radiusParam) {
    radius = radiusParam;
  }
  
  public void setAngle(double angleParam) {
    angle = angleParam;
  }
  
  @Override
  public String toString() {
    return "{type: bend, radius: " + radius + ", angle: " + angle + "}";
  }
}
