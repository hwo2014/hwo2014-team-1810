package messages;

import cars.CarId;

public class Spawn extends Message {
  private final String msgType = SPAWN;
  private final CarId data;
  
  private Spawn() {data = null;}
  
  public Spawn(CarId dataParam) {
    data = dataParam;
  }
  
  @Override
  public String getMsgType() {
    return msgType;
  }
  
  @Override
  public Object getData() {
    return data;
  }
}



