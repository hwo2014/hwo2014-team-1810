package messages;

import cars.CarId;

public class TurboEnd extends Message {
  private final String msgType = TURBOEND;
  private final CarId data;
  
  private TurboEnd() {data = null;}
  
  public TurboEnd(CarId dataParam) {
    data = dataParam;
  }
  
  @Override
  public String getMsgType() {
    return msgType;
  }
  
  @Override
  public Object getData() {
    return data;
  }
}


