package messages;

public class CreateRace extends Message {
  private final String msgType = CREATERACE;
  private final CustomRaceData data;
  
  private CreateRace() {data = null;}
  
  public CreateRace(CustomRaceData dataParam) {
    data = dataParam;
  }
  
  public CreateRace(BotId botId, String trackName, String password, int carCount) {
    data = new CustomRaceData(botId,trackName,password,carCount);
  }
  
  @Override
  public String getMsgType() {
    return msgType;
  }
  
  @Override
  public Object getData() {
    return data;
  }
}

