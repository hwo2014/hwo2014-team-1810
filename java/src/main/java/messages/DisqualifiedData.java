package messages;

import cars.CarId;

public class DisqualifiedData {
  private final CarId car;
  private final String reason;
  
  private DisqualifiedData() {car = null; reason = null;}
  
  public DisqualifiedData(CarId carParam, String reasonParam) {
    car = carParam;
    reason = reasonParam;
  }
  
  public CarId getCar() {
    return car;
  }
  
  public String reason() {
    return reason;
  }
  
  @Override
  public String toString() {
    return "{car: " + car + ", reason: " + reason + "}";
  }
}
