package cars;

import circuit.PiecePosition;

public class CarPosition {
  public CarId id;
  public double angle;
  public PiecePosition piecePosition;
  @Override
  public String toString() {
    return "{id: " + id + ", angle: " + angle + ", piecePosition: " + piecePosition + "}";
  }
}
