package cars;

public class CarId {
  private String name;
  private String color;
  
  public CarId() {}
  
  public CarId(String nameParam, String colorParam) {
    name = nameParam;
    color = colorParam;
  }
  
  public String getName() {
    return name;
  }
  
  public String getColor() {
    return color;
  }
  
  public void setName(String nameParam) {
    name = nameParam;
  }
  
  public void setColor(String colorParam) {
    color = colorParam;
  }
  
  @Override
  public String toString() {
    return "{name: \"" + name + "\", color: \"" + color + "\"}";
  }
  
  @Override
  public boolean equals(Object obj) {
    if (obj instanceof CarId) {
      CarId carId = (CarId)obj;
      return name.equals(carId.name) && color.equals(carId.color);
    } else return false;
  }
}
