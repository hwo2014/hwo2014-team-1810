package messages;

import java.util.ArrayList;

import cars.CarPosition;

public class CarPositions extends Message {
  private final String msgType = CARPOSITIONS;
  private final ArrayList<CarPosition> data;
  
  private CarPositions() {data = null;}
  
  public CarPositions(ArrayList<CarPosition> dataParam) {
    data = dataParam;
  }
  
  @Override
  public String getMsgType() {
    return msgType;
  }
  
  @Override
  public Object getData() {
    return data;
  }
}

