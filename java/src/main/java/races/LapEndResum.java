package races;

import cars.CarId;  

public class LapEndResum {
  public static class Ranking {
    public int overall;
    public int fastestLap;
    @Override
    public String toString() {
      return "{overall: " + overall + ", fastestLap: " + fastestLap + "}";
    }
  }
  
  private final CarId car;
  private final LapTime lapTime;
  private final Result raceTime;
  private final Ranking ranking;
  
  private LapEndResum() {car = null; lapTime = null; raceTime = null; ranking = null;}
  
  public LapEndResum(CarId carParam, LapTime lapTimeParam, Result raceTimeParam, Ranking rankingParam) {
    car = carParam;
    lapTime = lapTimeParam;
    raceTime = raceTimeParam;
    ranking = rankingParam;
  }
  
  public CarId getCar() {
    return car;
  }
  
  public LapTime getLapTime() {
    return lapTime;
  }
  
  public Result getRaceTime() {
    return raceTime;
  }
  
  public Ranking getRanking() {
    return ranking;
  }
  
  @Override
  public String toString() {
    return "{car: " + car + ", lapTime: " + lapTime + ", raceTime: " +
    raceTime + ", ranking: " + ranking + "}";
  }
  
}
