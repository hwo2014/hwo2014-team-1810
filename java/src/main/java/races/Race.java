package races;

import java.util.ArrayList;
import cars.Car;
import cars.CarPosition;
import cars.CarId;
import circuit.Track;

public class Race {
  private final Track track;
  private final ArrayList<Car> cars;
  private final RaceSession raceSession;
  
  public Race(Track trackParam, ArrayList<Car> carsParam, RaceSession raceSessionParam) {
    track = trackParam;
    cars = carsParam;
    raceSession = raceSessionParam;
    for (Car car : cars)
      car.setRace(this);
  }
  
  public Track getTrack() {
    return track;
  }
  
  public ArrayList<Car> getCars() {
    return cars;
  }
  
  public Car getCar(CarId carId) {
    for (Car car : cars) if (car.getId().equals(carId)) return car;
    return null;
  }
  
  public RaceSession getRaceSession() {
    return raceSession;
  }
  
  public void updatePosition(CarPosition carPosition) {
    for (Car car : cars) {
      if (car.getId().equals(carPosition.id)) {
        car.updatePosition(carPosition.piecePosition, carPosition.angle);
        break;
      }
    }
  }
  
  @Override
  public String toString() {
    return "{track:\n" + track + ",\ncars: " + cars +
            ",\nraceSession: " + raceSession + "}";
  }
}
