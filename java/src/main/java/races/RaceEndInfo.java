package races;

import java.util.ArrayList;

import cars.CarId;

public class RaceEndInfo {
  public static class ResultPair {
    public CarId car;
    public Result result;
    @Override
    public String toString() {
      return "{car: " + car + ", result: " + result + "}";
    }
  }
  
  public static class LapPair {
    public CarId car;
    public LapTime result;
    @Override
    public String toString() {
      return "{car: " + car + ", result: " + result + "}";
    }
  }
  
  private final ArrayList<ResultPair> results;
  private final ArrayList<LapPair> bestLaps;
  
  private RaceEndInfo() {results = null; bestLaps = null;}
  
  public RaceEndInfo(ArrayList<ResultPair> resultsParam, ArrayList<LapPair> bestLapsParam) {
    results = resultsParam;
    bestLaps = bestLapsParam;
  }
  
  public ArrayList<ResultPair> getResults() {
    return results;
  }
  
  public ArrayList<LapPair> getBestLaps() {
    return bestLaps;
  }
  
  @Override
  public String toString() {
    return "{results: " + results + ",\nbestLaps: " + bestLaps + "}";
  }
}
