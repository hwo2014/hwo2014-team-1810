package messages;

public class CustomRaceData {
  private final BotId botId;
  private final String trackName;
  private final String password;
  private final int carCount;
  
  private CustomRaceData() {botId = null; trackName = password = null; carCount = 0;}
  
  public CustomRaceData(BotId botIdParam, String trackNameParam,
  String passwordParam, int carCountParam) {
    botId = botIdParam;
    trackName = trackNameParam;
    password = passwordParam;
    carCount = carCountParam;
  }
  
  public BotId getBotId() {
    return botId;
  }
  
  public String getTrackName() {
    return trackName;
  }
  
  public String getPassword() {
    return password;
  }
  
  public int getCarCount() {
    return carCount;
  }
  
  @Override
  public String toString() {
    return "{botId: " + botId + ", trackName: " + trackName + ", password: " + password +
    ", carCount: " + carCount + "}";
  }
}
