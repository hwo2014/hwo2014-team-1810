package races;

public class Result {
  private final int laps;
  private final int ticks;
  private final int millis;
  
  private Result() {laps = ticks = millis = 0;}
  
  public Result(int lapsParam, int ticksParam, int millisParam) {
    laps = lapsParam;
    ticks = ticksParam;
    millis = millisParam;
  }
  
  public int getLaps() {
    return laps;
  }
  
  public int getTicks() {
    return ticks;
  }
  
  public int getMillis() {
    return millis;
  }
  
  @Override
  public String toString() {
    return "{laps: " + laps + ", ticks: " + ticks + ", millis: " + millis + "}";
  }
}
