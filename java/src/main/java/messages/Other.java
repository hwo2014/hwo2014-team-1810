package messages;

public class Other extends Message {
  public final String msgType;
  public final Object data;
  
  private Other() {msgType = null; data = null;}
  
  public Other(String msgTypeParam, String dataParam) {
    msgType = msgTypeParam;
    data = dataParam;
  }
  
  @Override
  public String getMsgType() {
    return msgType;
  }
  
  @Override
  public Object getData() {
    return data;
  }
}

