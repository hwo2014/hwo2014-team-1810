package messages;

import races.LapEndResum;

public class LapFinished extends Message {
  private final String msgType = LAPFINISHED;
  private final LapEndResum data;
  
  private LapFinished() {data = null;}
  
  public LapFinished(LapEndResum dataParam) {
    data = dataParam;
  }
  
  @Override
  public String getMsgType() {
    return msgType;
  }
  
  @Override
  public Object getData() {
    return data;
  }
}




