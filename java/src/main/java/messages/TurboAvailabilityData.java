package messages;

public class TurboAvailabilityData {
  private final double turboDurationMilliseconds;
  private final int turboDurationTicks;
  private final double turboFactor;
  
  private TurboAvailabilityData() {turboDurationMilliseconds = turboFactor = 0; turboDurationTicks = 0;}
  
  public TurboAvailabilityData(double turboDurationMillisecondsParam,
  int turboDurationTicksParam, double turboFactorParam) {
    turboDurationMilliseconds = turboDurationMillisecondsParam;
    turboDurationTicks = turboDurationTicksParam;
    turboFactor = turboFactorParam;
  }
  
  public double getTurboDurationMilliseconds() {
    return turboDurationMilliseconds;
  }
  
  public int getTurboDurationTicks() {
    return turboDurationTicks;
  }
  
  public double getTurboFactor() {
    return turboFactor;
  }
  
  @Override
  public String toString() {
    return "{turboDurationMilliseconds: " + turboDurationMilliseconds +
    ", turboDurationTicks: " + turboDurationMilliseconds +
    ", turboFactor: " + turboFactor + "}";
  }
}
