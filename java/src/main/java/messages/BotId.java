package messages;

public class BotId {
  private String name;
  private String key;
  
  public BotId() {}
  
  public BotId(String nameParam, String keyParam) {
    name = nameParam;
    key = keyParam;
  }
  
  public String getName() {
    return name;
  }
  
  public String getKey() {
    return key;
  }
  
  public void setName(String nameParam) {
    name = nameParam;
  }
  
  public void setKey(String keyParam) {
    key = keyParam;
  }
  
  @Override
  public String toString() {
    return "{name: " + name + ", key: " + key + "}";
  }
}

