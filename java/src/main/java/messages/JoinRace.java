package messages;

public class JoinRace extends Message {
  private final String msgType = JOINRACE;
  private final CustomRaceData data;
  
  private JoinRace() {data = null;}
  
  public JoinRace(CustomRaceData dataParam) {
    data = dataParam;
  }
  
  public JoinRace(BotId botId, String trackName, String password, int carCount) {
    data = new CustomRaceData(botId,trackName,password,carCount);
  }
  
  @Override
  public String getMsgType() {
    return msgType;
  }
  
  @Override
  public Object getData() {
    return data;
  }
}

