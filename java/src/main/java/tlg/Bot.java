package tlg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.Properties;
import java.net.Socket;
import java.lang.reflect.Type;
import messages.*;

public abstract class Bot extends Thread {
  
  private final Properties gameProperties;
  private final String host;
  private final int port;
  private Socket socket;
  private PrintWriter writer;
  private BufferedReader reader;
  private boolean verboseIn;
  private boolean verboseOut;
  private int gameTick;
  
  public Bot(boolean verboseInParam, boolean verboseOutParam, String hostParam,
  int portParam, Properties properties) {
    gameProperties = properties;
    host = hostParam;
    port = portParam;
    verboseIn = verboseInParam;
    verboseOut = verboseOutParam;
  }
  
  private Message joinMessage() {
    Message retVal;
    String botName = gameProperties.getProperty("botName");
    String botKey = gameProperties.getProperty("botKey");
    boolean quickRace = Boolean.parseBoolean(gameProperties.getProperty("quickRace"));
    BotId botId = new BotId(botName,botKey);
    if (quickRace) retVal = new Join(botName,botKey);
    else {
      boolean createRace = Boolean.parseBoolean(gameProperties.getProperty("createRace"));
      String trackName = gameProperties.getProperty("trackName");
      String password = gameProperties.getProperty("password");
      int carCount = Integer.parseInt(gameProperties.getProperty("carCount"));
      retVal = new JoinRace(botId,trackName,password,carCount);
    }
    return retVal;
  }
  
  private void send(Message msg) {
    writer.println(msg.toJson());
    if (verboseOut) {
      String name = gameProperties.getProperty("botName");
      System.out.println("bot " + name + " - sent message:");
      System.out.println(msg);
    }
  }
  
  public void setVerboseIn(boolean verboseInParam) {
    verboseIn = verboseInParam;
  }
  
  public void setVerboseOut(boolean verboseOutParam) {
    verboseOut = verboseOutParam;
  }
  
  public boolean isVerboseIn() {
    return verboseIn;
  }
  
  public boolean isVerboseOut() {
    return verboseOut;
  }
  
  public String getHost() {
    return host;
  }
  
  public int getPort() {
    return port;
  }
  
  public Properties getProperties() {
    return gameProperties;
  }
  
  public int getGameTick() {
    return gameTick;
  }
  
  public abstract Message carPositionsFunc(CarPositions msg);
  public abstract Message crashFunc(Crash msg);
  public abstract Message dnfFunc(Disqualified msg);
  public abstract Message finishFunc(Finish msg);
  public abstract Message gameEndFunc(GameEnd msg);
  public abstract Message gameInitFunc(GameInit msg);
  public abstract Message gameStartFunc(GameStart msg);
  public abstract Message joinFunc(Join msg);
  public abstract Message lapFinishedFunc(LapFinished msg);
  public abstract Message spawnFunc(Spawn msg);
  public abstract Message tournamentEndFunc(TournamentEnd msg);
  public abstract Message turboAvailableFunc(TurboAvailable msg);
  public abstract Message turboEndFunc(TurboEnd msg);
  public abstract Message turboStartFunc(TurboStart msg);
  public abstract Message yourCarFunc(YourCar msg);
  public abstract Message otherFunc(Other msg);
  
  @Override
  public void run() {
    try {
      socket = new Socket(host,port);
      writer = new PrintWriter
        (new OutputStreamWriter(socket.getOutputStream(),"utf-8"),true);
      reader = new BufferedReader
        (new InputStreamReader(socket.getInputStream(),"utf-8"));
      
      Message join = joinMessage();
      send(join);
      
      System.out.println("bot " + join.getData() + " connected to " + host +
      ":" + port);
      
      String name = gameProperties.getProperty("botName");
      String line;
      while ((line = reader.readLine()) != null) {
        Message msgUncast = Message.fromJson(line);
        Integer msgTick = msgUncast.getGameTick();
        if (msgTick != null && msgTick > gameTick)
          gameTick = msgTick;
        if (verboseIn) {
          System.out.println("bot " + name + " - received message:");
          System.out.println(msgUncast);
        }
        switch (msgUncast.getMsgType()) {
            case Message.CARPOSITIONS: {
              CarPositions msg = (CarPositions)msgUncast;
              Message answer = carPositionsFunc(msg);
              if (answer != null) send(answer);
              break;
          } case Message.CRASH: {
              Crash msg = (Crash)msgUncast;
              Message answer = crashFunc(msg);
              if (answer != null) send(answer);
              break;
          } case Message.DNF: {
              Disqualified msg = (Disqualified)msgUncast;
              Message answer = dnfFunc(msg);
              if (answer != null) send(answer);
              break;
          } case Message.FINISH: {
              Finish msg = (Finish)msgUncast;
              Message answer = finishFunc(msg);
              if (answer != null) send(answer);
              break;
          } case Message.GAMEEND: {
              GameEnd msg = (GameEnd)msgUncast;
              Message answer = gameEndFunc(msg);
              if (answer != null) send(answer);
              break;
          } case Message.GAMEINIT: {
              GameInit msg = (GameInit)msgUncast;
              Message answer = gameInitFunc(msg);
              if (answer != null) send(answer);
              break;
          } case Message.GAMESTART: {
              gameTick = 0;
              GameStart msg = (GameStart)msgUncast;
              Message answer = gameStartFunc(msg);
              if (answer != null) send(answer);
              break;
          } case Message.JOIN: {
              Join msg = (Join)msgUncast;
              Message answer = joinFunc(msg);
              if (answer != null) send(answer);
              break;
          } case Message.LAPFINISHED: {
              LapFinished msg = (LapFinished)msgUncast;
              Message answer = lapFinishedFunc(msg);
              if (answer != null) send(answer);
              break;
          } case Message.SPAWN: {
              Spawn msg = (Spawn)msgUncast;
              Message answer = spawnFunc(msg);
              if (answer != null) send(answer);
              break;
          } case Message.TOURNAMENTEND: {
              TournamentEnd msg = (TournamentEnd)msgUncast;
              Message answer = tournamentEndFunc(msg);
              if (answer != null) send(answer);
              break;
          } case Message.TURBOAVAILABLE: {
              TurboAvailable msg = (TurboAvailable)msgUncast;
              Message answer = turboAvailableFunc(msg);
              if (answer != null) send(answer);
              break;
          } case Message.TURBOEND: {
              TurboEnd msg = (TurboEnd)msgUncast;
              Message answer = turboEndFunc(msg);
              if (answer != null) send(answer);
              break;
          } case Message.TURBOSTART: {
              TurboStart msg = (TurboStart)msgUncast;
              Message answer = turboStartFunc(msg);
              if (answer != null) send(answer);
              break;
          } case Message.YOURCAR: {
              YourCar msg = (YourCar)msgUncast;
              Message answer = yourCarFunc(msg);
              if (answer != null) send(answer);
              break;
          } default: {
              Other msg = (Other)msgUncast;
              Message answer = otherFunc(msg);
              if (answer != null) send(answer);
              break;
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      if (socket != null) try{socket.close();} catch(Exception e) {}
      if (writer != null) writer.close();
      if (reader != null) try{reader.close();} catch(Exception e) {}
    }
  }
  
}
